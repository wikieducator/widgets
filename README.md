WikiEducator Widgets
======

Simple widgets used at WikiEducator, including:

* addToTable: add a row to a data table in a wiki page
* courseRegistration: register for a course using a form in the wiki
* registrations: use the [Faye](http://faye.jcoglan.com/) server to update a "live" registration/country count in the wiki
* userProfile: encourage users to complete a rudimentary profile page


