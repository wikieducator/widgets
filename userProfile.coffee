form = [
  name: 'Name'
  label: 'Full name:'
  type: 'name'
  req: true
  note: 'Required'
  err: 'Please supply your full name'
 ,
   #name: 'Email'
   #label: 'Email address:'
   #type: 'text'
   #note: 'Publicly displayed in profile'
   name: 'Employer'
   label: 'Organization:'
   type: 'text'
   note: 'Employer, university, school, or other affiliation'
 ,
   name: 'Occupation'
   label: 'Role:'
   type: 'text'
   note: 'Occupation, title, student level, etc.'
 ,
   name: 'Nationality'
   label: 'Nationality:'
   type: 'flagc'
 ,
   name: 'Country'
   label: 'Country:'
   type: 'flagc'
   note: 'Normal residence'
 ,
   name: 'Languages'
   label: 'Languages:'
   type: 'text'
   note: 'Comma separated list of written fluencies'
 ,
   name: 'about'
   label: 'About:'
   type: 'textarea'
   note: 'Please enter at least a sentence or two describing your interests and background.'
]
infobox = [
  'Name'
  'Email'
  'Boxes'
  'Photo'
  'Employer'
  'Website'
  'Blog'
  'Occupation'
  'Nationality'
  'Languages'
  'Country'
  'Skype address'
  'Other roles'
  'WM'
]
formtitle = 'User Profile'
options = {}
rform = {}
autorow = []
bottom = false
tid = ''

entityMap =
  "&": "&amp;"
  "<": "&lt;"
  ">": "&gt;"
  '"': '&quot;'
  "'": '&#39;'
  "/": '&#x2F;'

countries = [
    "", "Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra",
    "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina",
    "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas",
    "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize",
    "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina",
    "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory",
    "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia",
    "Cameroon", "Canada", "Cape Verde", "Cayman Islands",
    "Central African Republic", "Chad", "Chile", "China",
    "Christmas Island",
    "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo",
    "Cook Islands", "Costa Rica",
    "Cote D'ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic",
    "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador",
    "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia",
    "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands",
    "Fiji", "Finland", "France", "French Guiana", "French Polynesia",
    "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany",
    "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe",
    "Guam", "Guatemala", "Guinea", "Guinea-bissau", "Guyana", "Haiti",
    "Heard Island and Mcdonald Islands", "Holy See",
    "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia",
    "Iran", "Iraq", "Ireland", "Israel", "Italy",
    "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati",
    "Korea, Democratic People's Republic of", "Korea, Republic of",
    "Kuwait", "Kyrgyzstan", "Lao People's Democratic Republic",
    "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya",
    "Liechtenstein", "Lithuania", "Luxembourg", "Macao",
    "Macedonia", "Madagascar", "Malawi",
    "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands",
    "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico",
    "Micronesia, Federated States of", "Moldova, Republic of",
    "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique",
    "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands",
    "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua",
    "Niger", "Nigeria", "Niue", "Norfolk Island",
    "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau",
    "Palestinian Territory", "Panama", "Papua New Guinea",
    "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal",
    "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation",
    "Rwanda", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia",
    "Saint Pierre and Miquelon", "Saint Vincent and The Grenadines",
    "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia",
    "Senegal", "Serbia and Montenegro", "Seychelles", "Sierra Leone",
    "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia",
    "South Africa", "South Georgia",
    "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard and Jan Mayen",
    "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic",
    "Taiwan", "Tajikistan",
    "Tanzania", "Thailand", "Timor-leste",
    "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia",
    "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu",
    "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom",
    "UNESCO", "USA",
    "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Viet Nam",
    "Virgin Islands, British", "Virgin Islands, U.S.", "Wallis and Futuna",
    "Western Sahara", "Yemen", "Zambia", "Zimbabwe"
  ]

# exceptions mapping country to flag name
flags =
  "Bahamas": "the Bahamas"
  "Brunei Darussalam": "Brunei"
  "Holy See": "the Vatican City"
  "Korea, Republic of": "South Korea"
  "Netherlands": "the Netherlands"
  "United Arab Emirates": "the United Arab Emirates"

escapeHTML = (s) ->
  return String(s).replace(/[&<>"'\/]/g, (s) ->
    entityMap[s])

api = (data, success, failure) ->
  data.action ||= 'query'
  data.format ||= 'json'
  $.ajax(
    url: window.wgServer + '/api.php'
    type: 'POST'
    data: data
    success: success
    failure: failure
  )

formElement = (x, ix) ->
  r = "<td class=\"mw-label\"><label for=\"WErb#{ix}\">#{x.label}</label></td>"

  ni = "name=\"WErb#{ix}\" id=\"WErb#{ix}\""
  switch x.type
    when "select"
      r += '<td class="mw-input"><select ' + ni + '>'
      for o in x.options
        if typeof o is 'string'
          [v, t, text, selected, disabled] = [o, o, o, false, false]
        else
          [v, t, text, selected, disabled] = [o.value, o.title, o.text, o.selected, o.disabled]
        r += "<option value=\"#{v}\" title=\"#{t}\""
        r += ' disabled="disabled"' if disabled
        r += ' selected' if selected
        r += ">#{text}</option>"
      r += "</select>
            <div style=\"font-size: smaller;\">#{x.note||'&nbsp;'}</div></td>"
    when "country", "flagc", "flagcl"
      r += '<td class="mw-input"><select ' + ni + '>'
      for o in countries
        [v, t, text, selected, disabled] = [o, o, o, false, false]
        r += "<option value=\"#{v}\" title=\"#{t}\""
        r += ' disabled="disabled"' if disabled
        r += ' selected' if selected
        r += ">#{text}</option>"
      r += "</select>
            <div style=\"font-size: smaller;\">#{x.note||'&nbsp;'}</div></td>"
    when "radio"
      r += '<td class="mw-input">'
      iy = 0
      for o in x.options
        checked = if iy is 0 then 'checked' else ''
        r += "<input type=\"radio\" #{ni} value=\"#{o}\" #{checked}>#{o}&nbsp;&nbsp;&nbsp;"
        iy++
      r += "<div style=\"font-size: smaller;\">#{x.note||'&nbsp;'}</div></td>"
    when "textarea"
      r += "<td class=\"mw-input\"><textarea #{ni} rows=4 cols=40></textarea><div style=\"font-size:smaller;\">#{x.note||'&nbsp;'}</div></td>"
    else
      r += "<td class=\"mw-input\"><input #{ni} type=\"text\" size=\"40\""
      r += ' disabled="disabled"' if x.disabled
      r += " /><div style=\"font-size: smaller;\">#{x.note||'&nbsp;'}</div></td>"

userProfile = (r) ->
  profile = [
    '{{Infobox_personal'
  ]
  for p in infobox
    do (p) ->
      if p is 'Email'
        #profile.push("|Email=#{r[p].replace('@', '-AT-')}")
        profile.push("|Email=")
      else
        profile.push("|#{p}=#{r[p] || ''}")
  profile.push('}}')
  profile.push('')
  profile.push(r.about)
  profile.join("\n")

submitForm = ->
  err = false
  ix = 0
  for element in form
    $es = $("#WErb#{ix}")
    v = $.trim($es.val())
    if element.req and v is ""
      $es.next().text(element.err).css('color', 'FireBrick')
      err = true
    else
      $es.next().html('&nbsp;')
      switch element.type
        when 'userpage'
          v = "[[User:#{wgUserName}|#{v}]]"
        when 'flagc'
          if v isnt ""
            if flags.hasOwnProperty(v)
              v = "{{FlagC|#{flags[v]}|#{v}}}"
            else:
              v = "{{FlagC|#{v}}}"
        when 'flagcl'
          if v isnt ""
            if flags.hasOwnProperty(v)
              v = "{{FlagCL|#{flags[v]}|#{v}}}"
            else:
              v = "{{FlagCL|#{v}}}"
      rform[element.name] = v
      autorow.push(v)
      if element.summary
        columnsummary = v
    ix++
  if err
    return false
  # hide submit, show spinner
  $('#weAddUserProfile').hide()
  $('#WErbSpinner').show()
  # request page and edit token
  api
    prop: 'info|revisions'
    intoken: 'edit'
    rvprop: 'content'
    titles: window.wgPageName
    (d) ->
      #console.log(d)
      pages = d?.query?.pages
      for p of pages
        token = pages[p].edittoken
        #console.log('token=', token)
        api
          action: 'edit'
          title: window.wgPageName
          summary: 'create user profile'
          text: userProfile(rform)
          token: token
          (d) ->
            #console.log('saved!')
            window.location = "#{window.wgServer}/#{window.wgPageName}?1"
          (d) ->
            #console.log('unable to save')
            alert("Unable to save profile.  Please try later.")
            #window.location = "#{window.wgServer}/#{window.wgPageName}?1"
        if rform.comment
          comment = abridge(rform.comment)
        ###
        api
          action: 'wenotes'
          notag: 'oeruesp'
          notext: "New #{rform.sport} (#{rform.focus}) resource: #{rform.title} #{rform.url} #{comment}"
          ->
            t = 1
          ->
            t = 2
        ###
        break
    ->
      pass = 1
  row = []
  row.push("\n|-")
  # make sure the URL starts with https?://
  url = rform.url
  if not /^https?:\/\//i.exec(url)
    url = "http://#{url}"
  row.push("[[User:#{rform.user}|#{rform.user_name}]]")
  row.push(rform.activity)
  row.push("[#{url} #{rform.title}]")
  row.push(rform.comment)
  row.push("\n")
  row = row.join("\n|") + "\n"
  # find table
  # insert row
  # save
  # if successful redirect
  return false

showForm = ->
  $('#weUserProfileDialog').dialog(
    height: 'auto'
    width: 'auto'
    position:
      my: 'right top'
      at: 'right top'
      of: '#ca-edit'
    title: formtitle
    show: 'slow')
  getUserName()
  dt = new Date()
  ds = "#{dt.getUTCFullYear()}-#{('0'+(dt.getUTCMonth()+1)).slice(-2)}-#{('0'+dt.getUTCDate()).slice(-2)} #{('0'+dt.getUTCHours()).slice(-2)}:#{('0'+dt.getUTCMinutes()).slice(-2)}"
  for x in form
    nd = "#WErb#{x.ix}"
    switch x.type
      when "user" then $(nd).val(wgUserName)
      when "date" then $(nd).val(ds.slice(0, 11))
      when "timestamp" then $(nd).val(ds)
  $('#fweUserProfile .mw-input').css('padding-bottom', '2ex')
  return false

getUserName = ->
  rform.user = window.wgUserName
  api
    meta: 'userinfo'
    uiprop: 'realname'
    (d) ->
      #console.log(d)
      if d.query?.userinfo
        userinfo = d.query.userinfo
        rform.user_name = userinfo.realname
        for x in form
          if x.type is "name" or x.type is "userpage"
            $("#WErb#{x.ix}").val(userinfo.realname)

weUserProfile = () ->
  formtitle = 'Create profile'
  dialogWidth = 600
  dialogHeight = 400
  bottom = true
  $('body').append("""
  <div id="weUserProfileDialog" style="display:none;">
    <form id="fweUserProfile">
      <table></table>
    </form>
  </div>
  """)
  ix = 0
  for element in form
    do (element, ix) ->
      $("#weUserProfileDialog > form > table").append('<tr>' +
        formElement(element, ix) + "</tr>\n")
        element.ix = ix
    ix++
  $('#weUserProfileDialog > form > table').append('''
  <tr><td>&nbsp;</td>
    <td><img id="WErbSpinner" src="/skins/common/images/ajax-loader.gif"
          height=16 width=16 style="display: none" /><button
          id="weAddUserProfile">Submit</button></td></tr>
  ''')
  showForm()
  $('#weAddUserProfile').click(submitForm)
  return false

jQuery ->
  window.weUserProfile = weUserProfile
  return

